####################
### MACOS BUILD ####
####################

OUT = anwesenheit
C   = clang
CC  = clang++
# export MACOSX_DEPLOYMENT_TARGET = 10.15
# WINCC = x86_64-w64-mingw32-g++

SRC_C    = $(wildcard ./*.c) $(wildcard ./src/*.c) ./glad/src/glad.c
SRC_CPP  = $(wildcard ./*.cpp) $(wildcard ./src/*.cpp) $(wildcard ./imgui-1.89.9/*.cpp) \
			$(wildcard ./imgui-1.89.9/backends/imgui_impl_opengl3.cpp ./imgui-1.89.9/backends/imgui_impl_sdl2.cpp)
# SRC_LIN_C   = ./nativefiledialog/src/nfd_common.c ./nativefiledialog/src/nfd_gtk.c
# SRC_LIN_CPP = $(wildcard ./src/*.cpp) ./glad/src/glad.c ./imgui-1.89.9/imgui.cpp ./imgui-1.89.9/imgui_demo.cpp ./imgui-1.89.9/imgui_draw.cpp ./imgui-1.89.9/imgui_tables.cpp ./imgui-1.89.9/imgui_widgets.cpp ./imgui-1.89.9/backends/imgui_impl_opengl3.cpp ./imgui-1.89.9/backends/imgui_impl_sdl2.cpp   -lSDL2 -ldl -lgtk-3 -lgdk-3

INCLUDE = -I ./SDL2.framework/Headers -I ./glad/include -I ./imgui-1.89.9 -I ./imgui-1.89.9/backends -I ./stb_image.h
FRAMEWORKS = -F /Library/Frameworks -framework SDL2 -framework AppKit -framework CoreFoundation -lcurl
RPATH = -rpath @executable_path/../Frameworks
IFR = $(INCLUDE) $(FRAMEWORKS) $(RPATH)
# INCLUDEADDLINUX = -I /usr/portage/media-libs/libsdl2/files -I ./glad/include -I ./imgui-1.89.9 -I ./imgui-1.89.9/backends -I/usr/include/gtk-3.0 -I/usr/include/pango-1.0 -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include -I/usr/lib64/libffi/include -I/usr/include/fribidi -I/usr/include/cairo -I/usr/include/pixman-1 -I/usr/include/freetype2 -I/usr/include/uuid -I/usr/include/libpng16 -I/usr/include/harfbuzz -I/usr/include/gdk-pixbuf-2.0 -I/usr/include/libmount -I/usr/include/blkid -I/usr/include/gio-unix-2.0 -I/usr/include/atk-1.0 -I/usr/include/at-spi2-atk/2.0 -I/usr/include/dbus-1.0 -I/usr/lib64/dbus-1.0/include -I/usr/include/at-spi-2.0 -pthread -lgtk-3 -lgdk-3 -lpangocairo-1.0 -lpango-1.0 -latk-1.0 -lcairo-gobject -lcairo -lgdk_pixbuf-2.0 -lgio-2.0 -lgobject-2.0 -lglib-2.0
SDL = ./SDL2.framework
# MINGW_PATH = /usr/local/x86_64-w64-mingw32
# MINGW_INCLUDE = $(MINGW_PATH)/include
# MINGW_LIB = $(MINGW_PATH)/lib

MACOSARMOBJ = $(SRC_C:.c=.macosarm.o) $(SRC_CPP:.cpp=.macosarm.o) $(SRC_OBJC:.m=.macosarm.o)
# MACOSX64OBJ = $(SRC_C:.c=.macosx64.o) $(SRC_CPP:.cpp=.macosx64.o) $(SRC_OBJC:.m=.macosx64.o)
# WINX64OBJ   = $(SRC_C:.c=.win64.o) $(SRC_CPP:.cpp=.win64.o) $(SRC_WIN_CPP:.cpp=.win64.o)
# LINX64OBJ   = $(SRC_LIN_C:.c=.linx64.o)
OBJ 		= $(MACOSARMOBJ)
# OBJ         = $(MACOSARMOBJ) $(MACOSX64OBJ) $(WINX64OBJ) $(LINX64OBJ)


.PHONY: run_macos_arm all clean
all: run_macos_arm

%.macosarm.o: %.c
	$(C) $(IFR) -c $< -o $@ -g
%.macosarm.o: %.m
	$(C) $(IFR) -c $< -o $@ -g
%.macosarm.o: %.cpp
	$(CC) -std=c++17 $(IFR) -c $< -o $@ -g
link_macos_arm: $(MACOSARMOBJ)
	$(CC) -v $(MACOSARMOBJ) -o $(OUT) $(IFR)
debug_macos_arm: $(MACOSARMOBJ)
	$(CC) $(MACOSARMOBJ) -o $(OUT) $(IFR) -g -O0
bundle_macos_arm: link_macos_arm
	mkdir -p "./bin/$(OUT).app"/Contents/{MacOS,Frameworks,Resources}
	cp -R "$(SDL)" "./bin/$(OUT).app/Contents/Frameworks/"
	cp Info.plist "./bin/$(OUT).app/Contents/"
	cp ./$(OUT) "./bin/$(OUT).app/Contents/MacOS/$(OUT)"
# 	cp mod2frag.icns "./bin/$(OUT).app/Contents/Resources/"
# 	cp -r ./shaderfiles "./bin/$(OUT).app/Contents/Resources/"
# 	cp vertex_shader.glsl "./bin/$(OUT).app/Contents/Resources/"
run_macos_arm: bundle_macos_arm
#	open ./bin/$(OUT).app
	./bin/$(OUT).app/Contents/MacOS/$(OUT)

clean:
	rm -f $(OBJ) $(OUT)
	rm -rf ./bin/*
	rm -f imgui.ini
	rm -f $(RPI_OBJ) $(RPI_OUT)


####################
#### RPI BUILD #####
####################

RPI_OUT = anwesenheit
RPI_CC  = g++

RPI_SRC_C    = $(wildcard ./*.c) $(wildcard ./src/*.c) ./glad/src/glad.c
RPI_SRC_CPP  = $(wildcard ./*.cpp) $(wildcard ./src/*.cpp) $(wildcard ./imgui-1.89.9/*.cpp) \
            $(wildcard ./imgui-1.89.9/backends/imgui_impl_opengl3.cpp ./imgui-1.89.9/backends/imgui_impl_sdl2.cpp)
RPI_OBJ      = $(RPI_SRC_C:.c=.o) $(RPI_SRC_CPP:.cpp=.o)

RPI_CFLAGS   = $(shell sdl2-config --cflags) -I./glad/include -I./imgui-1.89.9 -I./imgui-1.89.9/backends -D__UNIX__
RPI_LDFLAGS  = $(shell sdl2-config --libs) -ldl -lpthread -lcurl

rpi_all: $(RPI_OUT)

%.o: %.c
	$(RPI_CC) $(RPI_CFLAGS) -c $< -o $@

%.o: %.cpp
	$(RPI_CC) $(RPI_CFLAGS) -c $< -o $@

$(RPI_OUT): $(RPI_OBJ)
	$(RPI_CC) -o $(RPI_OUT) $(RPI_OBJ) $(RPI_LDFLAGS)
