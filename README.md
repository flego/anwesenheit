# Anwesenheitsanzeige für das GAF-Büro

## Funktionsweise
* Verbleibende Anwesenheit wird auf einem Raspberry Pi mit Touchscreen im Fachschaftszimmer per GUI eingestellt
* Eingebauter Timer streamt in regelmäßigen Zeitintervallen und bei einer Statusänderung den aktuellen Status mit cURL an 
    * einen Web-Server
    * SpaceAPI
    * Telegram

![Anwesenheits-Automat](./anwesenheit_automat.jpg)

## Build + Installation auf Raspberry Pi
* git clone https://gitlab.com/flego/anwesenheit
* cd anwesenheit
* make rpi_all
* export DISPLAY=:0
* ./anwesenheit &

## Build + Installation auf MacOS
* git clone https://gitlab.com/flego/anwesenheit
* cd anwesenheit
* make
