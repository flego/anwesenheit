#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <fcntl.h>     // TODO: needed?
#include <sys/stat.h>  // TODO: needed?
#include <curl/curl.h>
#include <thread>
#ifdef __UNIX__
    #include <fstream>
    #include <string>
#endif
#ifdef __APPLE__
    #include <sys/sysctl.h>
    #include <ctime>
#endif
// #include <sstream>

#include "imgui.h"  // ui
#include "backends/imgui_impl_opengl3.h"
#include "backends/imgui_impl_sdl2.h"
#ifdef __APPLE__
    #include <SDL.h> // For macOS
    #include <CoreFoundation/CoreFoundation.h>
#else
    #include <SDL2/SDL.h> // For other platforms
#endif
// #include <SDL_opengl.h>
#include "glad/glad.h" // Include GLAD

#include "load_texture.h"
#include "auto_con_test.h"
#include "timer.h"
#include "write_status_to_file.h"


int http_response_code_wiki = 0; 
int http_response_code_b038 = 0;
std::thread timerThread;
std::thread autoConnectionTestThread;
bool online = true;
std::string phrase_complete_tmp;
static const char* phrase_complete = "Büro aktuell zu";
static const char* phrase_complete_previous = "Büro aktuell zu";

#ifdef __UNIX__
uint64_t getSystemUptime() {
    std::ifstream uptime_file("/proc/uptime");
    double uptime_seconds;
    uptime_file >> uptime_seconds;
    return static_cast<uint64_t>(uptime_seconds);
}
#endif
#ifdef __APPLE__
uint64_t getSystemUptime() {
    struct timeval boottime;
    int mib[2] = {CTL_KERN, KERN_BOOTTIME};
    size_t size = sizeof(boottime);

    if (sysctl(mib, 2, &boottime, &size, NULL, 0) != -1) {
        time_t bsec = boottime.tv_sec, csec = time(NULL);
        return static_cast<uint64_t>(difftime(csec, bsec));
    }
    return 0;
}
#endif

std::string formatUptime(uint64_t seconds) {
    uint64_t days = seconds / 86400;
    uint64_t hours = (seconds % 86400) / 3600;
    uint64_t minutes = (seconds % 3600) / 60;
    uint64_t secs = seconds % 60;
    char buffer[80];
    // sprintf(buffer, "Uptime: %llu d %llu h %llu m %llu s", days, hours, minutes, secs);
    snprintf(buffer, sizeof(buffer), "Uptime: %llud %lluh %llum %llus", days, hours, minutes, secs);
    return std::string(buffer);
}

std::string format_timer(int seconds) {
    bool is_negative = seconds < 0;
    uint64_t abs_seconds = std::abs(seconds);  // Convert to absolute value for calculation

    uint64_t hours = (abs_seconds % 86400) / 3600;
    uint64_t minutes = (abs_seconds % 3600) / 60;
    uint64_t secs = abs_seconds % 60;

    char buffer[80];
    if (is_negative) {
        snprintf(buffer, sizeof(buffer), "Verbl. Anwesenh.: - %lluh %llum %llus", hours, minutes, secs);
    } else {
        snprintf(buffer, sizeof(buffer), "Verbl. Anwesenh.: %lluh %llum %llus", hours, minutes, secs);
    }
    
    return std::string(buffer);
}



int main() {
    // Initialize SDL
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) != 0) {
        printf("Error: %s\n", SDL_GetError());
        return -1;
    }

    // Setup window
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

    // Create window with graphics context
    SDL_Window* window = SDL_CreateWindow("Anwesenheit", 
        SDL_WINDOWPOS_CENTERED, 
        SDL_WINDOWPOS_CENTERED, 
        320, 
        480, 
    #ifdef __APPLE__
        SDL_WINDOW_OPENGL);
    #else
        SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN);
    #endif
        // SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN_DESKTOP);
    // TODO: recalibrate mouse position  (don't need with SDL_WINDOW_FULLSCREEN)
    SDL_GLContext gl_context = SDL_GL_CreateContext(window);
    SDL_GL_MakeCurrent(window, gl_context);
    SDL_GL_SetSwapInterval(1); // Enable vsync

    // Initialize GLAD
    if (!gladLoadGLLoader((GLADloadproc)SDL_GL_GetProcAddress)) {
        printf("Failed to initialize GLAD\n");
        return -1;
    }

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    // Setup Platform/Renderer bindings
    ImGui_ImplSDL2_InitForOpenGL(window, gl_context);
    ImGui_ImplOpenGL3_Init("#version 150");

    // Automatic connection test every minute
    autoConnectionTestThread = std::thread(auto_test_connection, &online);

    // Load images
    int my_image_width = 16;
    int my_image_height = 16;
    GLuint circle_r = 0;
    GLuint circle_g = 0;
    GLuint circle_o = 0;
    bool ret = LoadTextureFromFile("./img/circle-r.png", &circle_r, &my_image_width, &my_image_height);
    // IM_ASSERT(ret);
    std::cout << "LoadTextureFromFile ./img/circle-r.png returned " << ret << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(1));
    ret = LoadTextureFromFile("./img/circle-g.png", &circle_g, &my_image_width, &my_image_height);
    // IM_ASSERT(ret);
    std::cout << "LoadTextureFromFile ./img/circle-g.png returned " << ret << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(1));
    ret = LoadTextureFromFile("./img/circle-o.png", &circle_o, &my_image_width, &my_image_height);
    // IM_ASSERT(ret);
    std::cout << "LoadTextureFromFile ./img/circle-o.png returned " << ret << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(1));


    // style
    ImGuiStyle& style = ImGui::GetStyle();
    style.WindowTitleAlign = ImVec2(0.03f, 0.5f);
    style.WindowPadding = ImVec2(10, 10);
    style.FramePadding = ImVec2(4, 8);
    style.ItemSpacing = ImVec2(8, 8);
    style.ItemInnerSpacing = ImVec2(6, 14);  // TODO: finetune layout
    style.FrameRounding = 3.0f;
    style.ScrollbarSize = 20.0f;

    // Init targets
    // read telegram bot token and chat id from file
    init_telegram();

    // Main loop
    bool done = false;
    while (!done) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            ImGui_ImplSDL2_ProcessEvent(&event);
            if (event.type == SDL_QUIT) {
                std::cout << "Stopping any active countdowns" << std::endl;
                timer_active = false;
                if (timerThread.joinable()) {
                    timerThread.join();
                }
                std::cout << "Quitting" << std::endl;
                done = true;
            }
            if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(window))
                done = true;
        }

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplSDL2_NewFrame(window);
        ImGui::NewFrame();

        // Here goes the ImGui code to create GUI elements

        ImGui::Begin("B 038", NULL, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove  | ImGuiWindowFlags_NoResize); // non collapsible, non movable
        ImGui::SetWindowPos(ImVec2(0, 0), ImGuiCond_Once);  // Make this window full screen  // TODO: remove once
        ImGui::SetWindowSize(ImVec2(io.DisplaySize.x, io.DisplaySize.y), ImGuiCond_Once);  // TODO: remove once
        ImGui::SetWindowFontScale(1.3f);  // TODO: finetune layout

        if (ImGui::CollapsingHeader("Anwesenheit", ImGuiTreeNodeFlags_DefaultOpen))
        {
            ImGui::TextWrapped("Bestimme, wie lange noch der Status \"Büro aktuell "
                "offen\" auf der GAF-Webseite öffentlich angezeigt werden soll.");
            ImGui::Spacing();

            ImGui::Separator();

            // status
            ImGui::Spacing();
            ImGui::Columns(2, "table1", false); // 4-ways, with border
            ImGui::TextWrapped("Live-Status:"); 
            // Q: How to use SameLine() with a little padding?
            
            ImGui::SameLine(0.0f, 10.0f);
            // ImGui::TextWrapped("🔴 🟢 ⚫️ 🟡 Büro [aktuell | noch 3 Stunden] [offen|zu"); ImGui::NextColumn();  // TODO
            // ImGui::Image((void*)phrase_light_current, ImVec2(40, 40)); ImGui::NextColumn();
            // std::lock_guard<std::mutex> lock(circleMutex);
            // int hour_counter_plus_one = hour_counter + 1;

            // Backup previous phrase
            // std::string phrase_complete_tmp = phrase_complete;

            if (!online) {
                ImGui::Image((void*)(intptr_t)circle_r, ImVec2(16, 16));
            } else if (timer_active && !countup_mode) {
                ImGui::Image((void*)(intptr_t)circle_g, ImVec2(16, 16));
            } else {
                ImGui::Image((void*)(intptr_t)circle_o, ImVec2(16, 16));
            }
            ImGui::NextColumn();
            if (online) {
                if (countup_mode)  // count-up mode
                {   phrase_complete_tmp = "Büro noch " + std::to_string(abs(hour_counter)) + " bis " + 
                        std::to_string(abs(hour_counter - 1)) + " Stunden " + std::string(phrase_is_present);
                } else if (std::string(phrase_is_present) == "zu") {  // TODO unify
                    phrase_complete_tmp = "Büro aktuell " + std::string(phrase_is_present);
                } else if (transmit_duration) {  // prognose
                    phrase_complete_tmp = "Büro noch " + std::to_string(hour_counter) + " bis " + 
                        std::to_string(hour_counter + 1) + " Stunden " + std::string(phrase_is_present);
                } else {
                    phrase_complete_tmp = "Büro aktuell " + std::string(phrase_is_present);
                }
                
            } else {
                phrase_complete_tmp = "Keine Verbindung zum Server";
            }
            
            phrase_complete = phrase_complete_tmp.c_str();
            // Check if phrase_complete has changed with respect to phrase_complete_previous
            // Q: How to compare two const char*?

            // if (strcmp(phrase_complete, phrase_complete_previous) != 0) {   // TEMPORARY in write_status_to_file.h
            // Write the new phrase to file
            write_status(std::string(phrase_complete));
            // Send the new phrase to Telegram
            // send_telegram_message(std::string(phrase_complete));  // TODO: here and not in write_status_to_file.h
            // Update phrase_complete_previous
            phrase_complete_previous = phrase_complete;
            // }

            ImGui::TextWrapped(phrase_complete); ImGui::NextColumn();

            ImGui::Columns(1);
            ImGui::Spacing();

            ImGui::Separator();

            ImGui::Spacing();

            // Buttons
            ImVec2 button_sz(40, 40);  // TODO
            float spacing = ImGui::GetStyle().ItemInnerSpacing.x;
            ImGui::PushButtonRepeat(true);
            
            ImGui::TextWrapped("Verbleib. Zeit \nmit Anwesenheit:"); 

            ImGui::SameLine(0.0f, spacing);  // TODO: finetune layout
            if (ImGui::ArrowButton("##left", ImGuiDir_Down)) { 
                // modify remaining time
                // countdown mode
                if (seconds_remaining > 3600) {  // TODO seconds_remaining atomic
                    hour_counter--; 
                    seconds_remaining -= 3600;
                }
                // reset countdown
                else if (seconds_remaining > 0 && seconds_remaining <= 3600) {
                    hour_counter = 0; 
                    seconds_remaining = 0;
                    countup_mode = false;  // for extra safety
                    timer_active = false;
                }
                // count-up mode
                else if (seconds_remaining <= 0 && transmit_duration) {
                    hour_counter--; 
                    seconds_remaining -= 3600;
                    countup_mode = true;
                    timer_active = true;
                    // Start a new timer thread if there is none running yet or 
                    //      if the previous one has finished
                    if (is_thread_completed) {
                        if (timerThread.joinable()) {
                            timerThread.detach();  // Detach the thread if it is still running
                        }
                        is_thread_completed = false;  // Reset the flag
                        timerThread = std::thread(timer_start);  // Start a new timer
                    }
                }
            }

            ImGui::SameLine(0.0f, spacing);
            ImGui::TextWrapped(" %d h ", hour_counter);  // TODO: finetune layout

            ImGui::SameLine(250.0f, spacing);    // TODO: finetune layout
            if (ImGui::ArrowButton("##right", ImGuiDir_Up)) { 
                if (seconds_remaining >= 0 && seconds_remaining < 24 * 3600) {  
                    countup_mode = false;  // for extra safety
                    hour_counter++; 
                    // Activate timer (needed when seconds_remaining == 0)
                    timer_active = true;
                    seconds_remaining += 3600;
                    // Start a new timer thread if there is none running yet or 
                    //      if the previous one has finished
                    if (is_thread_completed) {
                        if (timerThread.joinable()) {
                            timerThread.detach();  // Detach the thread if it is still running
                        }
                        is_thread_completed = false;  // Reset the flag
                        timerThread = std::thread(timer_start);  // Start a new timer
                    }
                }
                // count-up mode reset
                else if (seconds_remaining >= -3600 && seconds_remaining < 0) {
                    hour_counter = 0; 
                    seconds_remaining = 0;
                    countup_mode = false;
                    timer_active = false;
                }
                // count-up mode shorten remaining time
                else if (seconds_remaining < -3600) {
                    hour_counter++; 
                    seconds_remaining += 3600;
                }
            }

            ImGui::PopButtonRepeat();
            ImGui::Spacing();

            ImGui::Separator();

            ImGui::Spacing();
            ImGui::Checkbox(" Prognose anzeigen", &transmit_duration);
            ImGui::Spacing();
            ImGui::Spacing();
        }

        if (ImGui::CollapsingHeader("System"))
        {
            ImGui::Spacing();
            if (ImGui::Button("Statistiken")) {
                // ImGui::SetNextWindowBgAlpha(1.0f);
                ImGui::OpenPopup("Statistiken");
            }
            ImGui::SameLine(0.0f, 25.0f);
            if (ImGui::Button("Energieoptionen")) {
                // ImGui::SetNextWindowBgAlpha(1.0f);
                ImGui::OpenPopup("Energieoptionen");
                // Always center this window when appearing
                // ImVec2 center = ImGui::GetMainViewport()->GetCenter();
                // ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
            }
            ImGui::Spacing();
        }

        // additional (modal) windows
        // stats
        ImGui::SetNextWindowBgAlpha(1.0f);  // TODO: remove white edge
        if (ImGui::BeginPopupModal("Statistiken", NULL, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize)) {
            ImGui::SetWindowPos(ImVec2(0, 0), ImGuiCond_Once);  // Make this window full screen  // TODO: remove once
            ImGui::SetWindowSize(ImVec2(io.DisplaySize.x, io.DisplaySize.y), ImGuiCond_Once);  // TODO: remove once

            ImGui::Spacing();
            uint64_t uptime = getSystemUptime();
            std::string uptimeStr = formatUptime(uptime);
            ImGui::Text("%s", uptimeStr.c_str());
            ImGui::Spacing();
            if (ImGui::Button("Verbindungstest", ImVec2(160,0))) { 
                    test_connection("https://wikipedia.org", &http_response_code_wiki);
                    test_connection("https://fs.lmu.de/", &http_response_code_b038);  // TODO
            }
            if (http_response_code_wiki > 0 || http_response_code_b038 > 0) {
                ImGui::TextWrapped("HTTP Response from \nwikipedia.org:");
                if (http_response_code_wiki > 0 && http_response_code_wiki < 400) {
                    ImGui::Image((void*)(intptr_t)circle_g, ImVec2(16, 16));
                } else { 
                    ImGui::Image((void*)(intptr_t)circle_r, ImVec2(16, 16));
                }
                ImGui::SameLine(0.0f, 15.0f);
                ImGui::TextWrapped("%d", http_response_code_wiki);

                ImGui::TextWrapped("HTTP Response from \nfs.lmu.de/:");
                if (http_response_code_b038 > 0 && http_response_code_b038 < 400) {
                    ImGui::Image((void*)(intptr_t)circle_g, ImVec2(16, 16));
                    online = true;
                } else { 
                    ImGui::Image((void*)(intptr_t)circle_r, ImVec2(16, 16));
                    online = false;
                }
                ImGui::SameLine(0.0f, 15.0f);
                ImGui::TextWrapped("%d", http_response_code_b038);

                ImGui::Spacing();

                ImGui::TextWrapped("Die Datenübertragung erfolgt an hund.fs.lmu.de");
                ImGui::Spacing();
                if (ImGui::Button("Cache leeren", ImVec2(160, 0))) {
                    http_response_code_wiki = 0;
                    http_response_code_b038 = 0;
                }
            }
            // std::cout << "HTTP response from wikipedia:  " << http_response_code;
            ImGui::Spacing();
            ImGui::Separator();

            ImGui::Spacing();
            ImGui::TextWrapped(format_timer(seconds_remaining).c_str());
            ImGui::TextWrapped("Abwesenheitsmodus: %s", countup_mode ? "aktiv" : "inaktiv");
            ImGui::Spacing();
            ImGui::Separator();

            ImGui::Spacing();
            if (ImGui::Button("Zurück", ImVec2(120, 0))) { ImGui::CloseCurrentPopup(); }
            ImGui::EndPopup();
        }

        // energy options
        ImGui::SetNextWindowBgAlpha(1.0f);  // TODO: remove white edge
        if (ImGui::BeginPopupModal("Energieoptionen", NULL, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoMove  | ImGuiWindowFlags_NoResize)) {
            ImGui::SetWindowPos(ImVec2(0, 0), ImGuiCond_Once);  // Make this window full screen  // TODO: remove once
            ImGui::SetWindowSize(ImVec2(io.DisplaySize.x, io.DisplaySize.y), ImGuiCond_Once);  // TODO: remove once

            ImGui::Spacing();
            ImGui::TextWrapped("Du sollst nicht das Gerät ausschalten.");
            ImGui::Spacing();

            ImGui::Separator();

            ImGui::Spacing();
            float availableWidth = ImGui::GetContentRegionAvail().x;
            if (ImGui::Button("Zurück", ImVec2(availableWidth, 80))) { 
		    ImGui::CloseCurrentPopup(); 
	        }
            if (ImGui::Button("Bildschirm dimmen", ImVec2(availableWidth, 80))) { 
		    ImGui::CloseCurrentPopup(); 
	        }
            if (ImGui::Button("Neustart", ImVec2(availableWidth, 80))) { 
		    ImGui::CloseCurrentPopup(); 
		    // Call the shutdown command
                    system("sudo shutdown -r now");
                    return 0;
	         }
            if (ImGui::Button("Ausschalten", ImVec2(availableWidth, 80))) { 
		    // ImGui::CloseCurrentPopup(); 
                    // Call the shutdown command
                    system("sudo shutdown now");
                    return 0;
	        }
            ImGui::EndPopup();
        }

        ImGui::End();

        // ImGui::ShowDemoWindow();

        // Rendering
        ImGui::Render();
        glViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);
        glClearColor(0.45f, 0.55f, 0.60f, 1.00f);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        SDL_GL_SwapWindow(window);

    }

    // Cleanup imgui

    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();

    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
