#pragma once
#include <chrono>
#include <functional>
#include <iostream>
#include <atomic>
#include <mutex>
#include <thread>

void test_connection(const char* url, int* response_code) {
    CURL* curl = curl_easy_init();
    if (curl) {
        CURLcode res;

        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_NOBODY, 1L);
        res = curl_easy_perform(curl);

        if (res == CURLE_OK) {
            long temp_response_code;
            curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &temp_response_code);
            *response_code = static_cast<int>(temp_response_code);
        }

        curl_easy_cleanup(curl);
    }
}

void auto_test_connection(bool* online) {
    int response_code = 0;
    // test_connection("https://gaf.fs.lmu.de/b038", &response_code); // TODO
    test_connection("https://fs.lmu.de/", &response_code);
    std::cout << "Auto connection test response code: " << response_code << std::endl;
    if (response_code >= 200 && response_code < 400) {
        *online = true;
    }
    else {
        *online = false;
    }

    // Sleep for 60 seconds
    std::this_thread::sleep_for(std::chrono::seconds(60));
    // Restart the function
    auto_test_connection(online);
}

// Callback function:
// void timer_end() {
//     // do something
//     // std::lock_guard<std::mutex> lock(mtx);  // Don't need, non blocking write
//     std::cout << "doing funny things" << std::endl;

//     // Set the thread completed flag
//     is_thread_completed = true;
    
// }

