#pragma once
#include <fstream>
#include <string>
#include <iostream>
#include <curl/curl.h>

std::string previous_status;

void send_spaceapi_message();
void send_telegram_message(const std::string& message);
std::string chat_id;
std::string bot_token;


void init_telegram() {

    // Open the file
    std::ifstream file("telegram_credentials");

    // Check if the file is open
    if (file.is_open()) {
        // Read the first line
        if (std::getline(file, chat_id)) {
            std::cout << "Telegram chat id: " << chat_id << std::endl;
        }

        // Read the second line
        if (std::getline(file, bot_token)) {
            std::cout << "Telegram bot token: " << bot_token << std::endl;
        }

        // Close the file
        file.close();
    } else {
        std::cerr << "Unable to open the file" << std::endl;
    }

}

void write_status(std::string status) {  // TODO: duplicate check earlier
    if (previous_status == status) {
        // Don't write to file
        // std::cout << "Status has not changed, not writing to file." << std::endl;
        return;
    }
    else {                              // Status has changed, second check seems to work
        // Update previous status
        previous_status = status;

        // Send message to Telegram        // TODO: move to main
        send_telegram_message(status);  // TODO: duplicate check earlier

        // Send message to SpaceAPI        // TODO: move to main
        send_spaceapi_message();
    }

    // Create an ofstream object for file writing
    #ifdef __UNIX__
    std::ofstream file("./anwesenheit_status.txt");
    #endif
    #ifdef __APPLE__
    std::ofstream file("./anwesenheit_status.txt");
    #endif

    // Check if the file is open and ready for writing
    if (file.is_open()) {
        // Write the string to the file
        file << status << std::endl;
        std::cout << "Wrote status to file: " << status << std::endl;

        // Close the file
        file.close();
    } else {
        // Handle the error (e.g., file couldn't be opened)
        std::cerr << "Unable to open anwesenheit status file for writing." << std::endl;
    }
}

void send_telegram_message(const std::string& message) {  // TODO: duplicate check earlier
    CURL *curl = curl_easy_init();
    if(curl) {
        std::string url = "https://api.telegram.org/bot" + bot_token + "/sendMessage?chat_id=" + chat_id + "&text=" + curl_easy_escape(curl, message.c_str(), message.length());
        std::cout << "Sending message to Telegram: " << url << std::endl;
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

        CURLcode res = curl_easy_perform(curl);
        if(res != CURLE_OK) {
            std::cerr << "curl_easy_perform() failed: " << curl_easy_strerror(res) << std::endl;
        }

        curl_easy_cleanup(curl);
    }
}

void send_spaceapi_message() {
    std::string state_open = (timer_active && !countup_mode) ? "true" : "false";
    
    std::string json = R"({
    "api": "0.13",
    "api_compatibility": ["14"],
    "space": "Gruppe aktiver Fachschaftika an der LMU München",
    "logo": "https://gaf.fs.lmu.de/kontakt/gaf-logo/image",
    "url": "https://gaf.fs.lmu.de/",
    "location": {
        "address": "Theresienstraße 39, 80333 München, Germany",
        "lon": 48.148,
        "lat": 11.573
    },
    "contact": {
        "email": "gaf@fs.lmu.de",
        "issue_mail": "root@fs.lmu.de",
        "irc": "irc://irc.libera.chat/gaf",
        "ml": "gaf-news@fs.lmu.de",
        "phone": "+49 89 2180 4382"
    },
    "issue_report_channels": ["issue_mail"],
    "state": {
        "open": )" + state_open + R"(
    },
    "projects": [
        "https://opha.se",
        "https://git.fs.lmu.de"
    ]
    })";

    // Output the JSON string
    std::cout << json << std::endl;
}

// int main() {
    

//     send_telegram_message(message);
//     return 0;
// }