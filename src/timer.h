#pragma once
#include <chrono>
#include <functional>
#include <iostream>
#include <atomic>
#include <mutex>
#include <thread>

void timer_start();
void timer_end();

std::atomic<bool> is_thread_completed(true);
std::mutex mtx;  // Mutex for thread-safe output
std::mutex circleMutex;  // Mutex for thread-safe output

bool countup_mode = false;
bool timer_active = false;
static signed int hour_counter = 0;
static signed int seconds_remaining = 0;
static const char* phrase_is_present = "zu";
bool transmit_duration = false;

void timer_start() {
    using namespace std::chrono;

    // Set the timer to active
    timer_active = true;
    // Set phrased status
    // std::lock_guard<std::mutex> lock(circleMutex);
    // circle_current = circle_g;
    if (countup_mode) {
        phrase_is_present = "zu";
    }
    else {
        phrase_is_present = "offen";
    }

    while (timer_active) {
        // Sleep for 1 second
        std::this_thread::sleep_for(seconds(1));

        // Update the time remaining
        if (countup_mode) {
            phrase_is_present = "zu";  // for extra safety
            seconds_remaining++;
            // end count-up timer when transmit_duration is turned off
            // or when seconds_remaining is at least 0
            if (transmit_duration == false || seconds_remaining >= 0) {
                timer_end();
            }
        } else {
            phrase_is_present = "offen";  // for extra safety
            seconds_remaining--;
            // end count-up timer wseconds_remaining is max 0
            if (seconds_remaining <= 0) {
                timer_end();
            }
        }

        // Output the time remaining
        // std::cout << "Time remaining: " << seconds_remaining << " seconds" << std::endl;

        // Update the hour counter
        // set hour_counter to full int devision of seconds_remaining / 3600
        hour_counter = seconds_remaining / 3600;
        // std::cout << "Hour counter now at " << hour_counter << std::endl;
    }
}

// Callback function:
void timer_end() {
    // do something
    // std::lock_guard<std::mutex> lock(mtx);  // Don't need, non blocking write
    // std::cout << "doing funny things" << std::endl;

    // Stop the timer
    timer_active = false;
    // Reset the countdown mode
    countup_mode = false;
    // Set the phrased status
    phrase_is_present = "zu";
    // std::lock_guard<std::mutex> lock(circleMutex);
    // circle_current = circle_o;

    // std::lock_guard<std::mutex> lock(mtx);
    std::cout << "Countdown stopped" << std::endl;

    // reset the hour counter
    hour_counter = 0;
    // reset the seconds remaining
    seconds_remaining = 0;
    // reset the transmit duration flag
    transmit_duration = false;

    // Set the thread completed flag
    is_thread_completed = true;
    
}

