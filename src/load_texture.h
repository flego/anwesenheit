#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

// Simple helper function to load an image into a OpenGL texture with common settings
bool LoadTextureFromFile(const char* filename, GLuint* out_texture, int* out_width, int* out_height)
{
    // Load from file
    int image_width = 0;
    std::cout << "Loading texture pi debug 1" << std::endl;
    int image_height = 0;
    std::cout << "Loading texture pi debug 2" << std::endl;
    unsigned char* image_data = stbi_load(filename, &image_width, &image_height, NULL, 4);
    std::cout << "Loading texture pi debug 3" << std::endl;
    if (image_data == NULL) {
        if (stbi_failure_reason()) std::cout << stbi_failure_reason();
        return false;
    }
    std::cout << "Loading texture pi debug 4" << std::endl;

    // Create a OpenGL texture identifier
    GLuint image_texture;
    std::cout << "Loading texture pi debug 5" << std::endl;
    glGenTextures(1, &image_texture);
    std::cout << "Loading texture pi debug 6" << std::endl;
    glBindTexture(GL_TEXTURE_2D, image_texture);
    std::cout << "Loading texture pi debug 7" << std::endl;

    // Setup filtering parameters for display
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    std::cout << "Loading texture pi debug 8" << std::endl;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    std::cout << "Loading texture pi debug 9" << std::endl;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // This is required on WebGL for non power-of-two textures
    std::cout << "Loading texture pi debug 10" << std::endl;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); // Same
    std::cout << "Loading texture pi debug 11" << std::endl;

    // Upload pixels into texture
#if defined(GL_UNPACK_ROW_LENGTH) && !defined(__EMSCRIPTEN__)
    std::cout << "Loading texture pi debug 12" << std::endl;
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    std::cout << "Loading texture pi debug 13" << std::endl;
#endif
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image_width, image_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data);
    std::cout << "Loading texture pi debug 14" << std::endl;
    stbi_image_free(image_data);
    std::cout << "Loading texture pi debug 15" << std::endl;

    *out_texture = image_texture;
    std::cout << "Loading texture pi debug 16" << std::endl;
    *out_width = image_width;
    std::cout << "Loading texture pi debug 17" << std::endl;
    *out_height = image_height;
    std::cout << "Loading texture pi debug 18" << std::endl;

    return true;
}